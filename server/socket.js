const httpServer = require('http').createServer();

const options = {
  cors: {
    origin: '127.0.0.1',
    methods: ['GET', 'POST'],
    credential: true
  }
};

const io = require('socket.io')(httpServer, options);

const sockets = [];

io.on('connection', (socket) => {
  sockets[socket.id] = socket;
  // eslint-disable-next-line no-console
  console.log(socket);
});

httpServer.listen(3000, () => {
  // eslint-disable-next-line no-console
  console.log('listening on *:3000');
});
