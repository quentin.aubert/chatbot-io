export default class Message {
  constructor(msg, author = 'me', createdAt = Date.now(), updatedAt = Date.now()) {
    this.msg = msg;
    this.author = author;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  setAuthor(author) {
    this.author = author;
  }

  getAuthor() {
    return this.author;
  }

  setMsg(msg) {
    this.msg = msg;
  }

  getMsg() {
    return this.msg;
  }

  setCreatedAt(createdAt) {
    this.created_at = createdAt;
  }

  getCreatedAt() {
    return this.createdAt;
  }

  setUpdatedAt(updatedAt) {
    this.updated_at = updatedAt;
  }

  getUpdatedAt() {
    return this.updatedAt;
  }
}
