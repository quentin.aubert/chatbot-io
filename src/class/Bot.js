import Message from './Message';
import Action from './Action';

export default class Bot {
  constructor(entity) {
    this.entity = entity;
  }

  setName(name) {
    this.entity.name = name;
  }

  getName() {
    return this.entity.name;
  }

  getActions() {
    return this.entity.actions;
  }

  getAvatar() {
    return this.entity.avatar;
  }

  setAvatar(avatar) {
    this.entity.avatar = avatar;
  }

  execActions(chat, userMessage) {
    const messageTable = userMessage.split(' ');
    const cmd = messageTable[0].toLowerCase();
    const param = messageTable[1];
    const helpCmds = ['?', 'help', 'aide'];

    if (helpCmds.includes(cmd)) {
      const allActions = this.getActions();
      const msg = new Message('', this);
      allActions.forEach((action) => {
        const { title, desc, cmds } = action;
        let commands = ' [';
        cmds.forEach((command, key) => { commands += command + (key !== action.cmds.length - 1 ? ',' : ''); });
        commands += `] (${action.param}) : `;
        const line = title + commands + desc;
        msg.setMsg((msg.getMsg() === '' ? '' : `${msg.getMsg()}<br>`) + line);
      });
      document.querySelector('#messages').innerHTML += chat.receiveMessage(msg);
    } else {
      this.getActions().map((action) => new Action(cmd, param, chat, this, action));
    }
  }
}
