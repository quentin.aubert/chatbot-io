import Message from './Message';

const io = require('socket.io-client');

const socket = io.connect('http://127.0.0.1:3000');
// eslint-disable-next-line no-console
console.log(socket);

export default class Chat {
  constructor(contacts) {
    this.contacts = contacts;
    this.app = document.querySelector('#app');
    this.run();
  }

  avatar(user) {
    if (user.getAvatar() === '') {
      let letters = user.getName().split(' ').map((i) => i.charAt(0));
      letters = letters.toString().toUpperCase();
      const render = letters.split(',').join('');
      return `
        <div class="bg-dark text-white avatar-number rounded-circle">
          <p class="avatar-txt">${render.substring(0, 2)}</p>
        </div>`;
    }
    return `
      <figure class="bg-dark text-white avatar-number rounded-circle">
        <img class="avatar-contact" src="./src/img/${user.getAvatar()}" alt="Avatar de ${user.getName()}">
      </figure>`;
  }

  transformDate(instantdate) {
    const date = new Date(instantdate).toLocaleString();
    return date;
  }

  renderHeader() {
    return ` 
      <header class="col-12">
        <nav class="navbar navbar-black bg-black">
          <div class="container-fluid">
            <span class="navbar-brand mb-0 h1 text-white"><strong>ChatBot.io</strong> | Version 1.0.0</span>
          </div>
        </nav>
      </header>
    `;
  }

  receiveMessage(msg) {
    return `
      <div class="message--received row">
      <div class="col-6">
        <div class="card mb-2">
          <h5 class="card-header">
            <span>${msg.getAuthor().getName()}</span>
          </h5>
          <div class="card-body">
            <p class="card-text">${msg.getMsg()}<p>
          </div>
          <div class="card-footer text-muted">
            ${this.transformDate(msg.getCreatedAt())}
          </div>
        </div>
      </div>
      <div class="col-6"></div>
    </div>
    `;
  }

  sendMessage(msg) {
    return `
      <div class="message--send row">
          <div class="col-6"></div>
          <div class="col-6">
            <div class="card mb-6">
              <h5 class="card-header">
                <span>${msg.getAuthor()}</span>
              </h5>
              <div class="card-body">
                <p class="card-text">${msg.getMsg()}</p>
              </div>
              <div class="card-footer text-muted">
                ${this.transformDate(msg.getCreatedAt())}
              </div>
            </div>
          </div>
      </div>
    `;
  }

  renderMessages(msg = []) {
    return `
      <div class="col-9">
        <div class="chat p-4 mh-80 ">
          <div class="messages" id ="messages">
            ${msg.map((message) => (message.getAuthor().toLowerCase() === 'me' ? this.sendMessage(message) : this.receiveMessage(message)).join(''))}
          </div>
        </div>
        <form class="row bg-white input-bottom" id="msg-form">
          <div class="col-11">
            <input type="text" class="form-control" id="msg" placeholder="Write a message">
          </div>
          <div class="col-1">
            <button type="submit" class="btn btn-primary mb-2 w-100">Send</button>
          </div>
        </form>
      </div>
    `;
  }

  addContact(contact) {
    const name = contact.getName();
    return `
      <a href="#" class="list-group-item list-group-item-action list-group-item-white">
        <div class="contact-avatar">${this.avatar(contact)}
            <div class="d-flex align-items-center justify-content-between mb-1">
              <h6>${name}</h6>
            </div>
        </div>
      </a>
    `;
  }

  renderContacts(contacts = []) {
    return `
      <div class="col-3 mt-1 text-black">
          <div class="row mb-1">
            <div class="list-group">
              <a class="list-group-item font-weight-bold bg-green">
                <h6>Bot en lignes <span class="badge rounded-pill bg-primary">${contacts.length}</span></h6>
              </a>
            </div>
          </div>
          ${contacts.map((contact) => this.addContact(contact)).join('')}
      </div>
    `;
  }

  writingMessage() {
    const form = document.getElementById('msg-form');
    form.addEventListener('submit', (e) => {
      e.preventDefault();
      const msg = document.getElementById('msg').value;
      if (msg !== '') {
        const newMessage = new Message(msg);

        document.getElementById('messages').innerHTML += this.sendMessage(newMessage);

        this.contacts.map((contact) => contact.execActions(this, msg));

        document.getElementById('msg').value = '';
      }
    });
  }

  run() {
    this.app.innerHTML += this.renderHeader();
    this.app.innerHTML += `
      <div class="container-fluid p-0">
        <div class="row">
          ${this.renderContacts(this.contacts)}
          ${this.renderMessages()}
        </div>
      </div>
    `;
    this.writingMessage();
  }
}
