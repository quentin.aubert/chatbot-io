import Message from './Message';

export default class Action {
  constructor(cmd, param, chat, bot, actions) {
    const {
      title,
      desc,
      cmds,
      action
    } = actions;

    this.title = title;
    this.desc = desc;
    this.cmds = cmds;
    this.action = action;
    this.messages = document.querySelector('#messages');

    this.runAction(cmd, param, chat, bot);
  }

  async runAction(cmd, param, chat, bot) {
    if (this.cmds.includes(cmd)) {
      const returnData = this.action(param);
      const msg = new Message(returnData, bot);

      if (returnData instanceof Promise) {
        returnData.then((value) => {
          msg.setMsg(value);
          this.messages.innerHTML += chat.receiveMessage(msg);
        }, (error) => {
          msg.setMsg(error);
          this.messages.innerHTML += chat.receiveMessage(msg);
        });
      } else {
        setTimeout(() => {
          this.messages.innerHTML += chat.receiveMessage(msg);
        }, 500);
      }
    }
  }
}
