import Chat from './class/Chat';
import Bot from './class/Bot';

import './css/index.scss';

const data = [
  {
    name: 'Pokédex',
    avatar: 'pokedex.png',
    actions: [
      {
        title: 'Salutation',
        desc: 'Hello Pokedex',
        cmds: ['hello', 'Hello', 'HELLO', 'Hi', 'hi'],
        param: '',
        action() {
          return 'Hello, i\'m Pokédex';
        }
      }, {
        title: 'Pokemon',
        desc: 'Give informations about a pokemon',
        cmds: ['info', 'pkmninfo', 'pkmn', 'pokemon'],
        param: 'pokemonName',
        async action(pokemonName) {
          return fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`)
            .then((res) => res.json())
            .then((dataReturn) => `Informations about <strong>${dataReturn.name}</strong> :<br> Pokedex ID : ${dataReturn.id} <br> The base experience gained for defeating this Pokémon. : ${dataReturn.base_experience} xp<br> <img src="${dataReturn.sprites.other['official-artwork'].front_default}" alt="${pokemonName} artwork" style="max-width: 220px; max-height: 220px">`)
            .catch((error) => error);
        }
      }, {
        title: 'Shiny',
        desc: 'Show Shiny Sprite of a Pokémon',
        cmds: ['Shiny', 'shiny'],
        param: 'pokemon',
        async action(pokemonName) {
          return fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`)
            .then((res) => res.json())
            .then((dataReturn) => `<img src="${dataReturn.sprites.front_shiny}" alt="${pokemonName} artwork" style="max-width: 220px; max-height: 220px">`)
            .catch((error) => error);
        }
      }
    ]
  }, {
    name: 'Meme Generator',
    avatar: 'meme.png',
    actions: [
      {
        title: 'Salutation',
        desc: 'Hello Meme Generator',
        cmds: ['hello', 'Hello', 'HELLO', 'Hi', 'hi'],
        param: '',
        action() {
          return 'Hello, i\'m Meme Generator';
        }
      }, {
        title: 'Meme',
        desc: 'Meme generator',
        cmds: ['meme', 'generate', 'Meme'],
        param: '',
        async action() {
          return fetch('https://meme-api.herokuapp.com/gimme')
            .then((res) => res.json())
            .then((dataReturn) => `<img src="${dataReturn.url}" alt="" style="max-width: 220px; max-height: 220px">`)
            .catch((error) => error);
        }
      }, {
        title: 'Meme Reddit',
        desc: 'Meme generator from specific reddit subject',
        cmds: ['rd', 'reddit', 'Reddit'],
        param: 'subject',
        async action(subjectName) {
          return fetch(`https://meme-api.herokuapp.com/gimme/${subjectName}`)
            .then((res) => res.json())
            .then((dataReturn) => `<img src="${dataReturn.url}" alt="" style="max-width: 220px; max-height: 220px">`)
            .catch((error) => error);
        }
      }
    ]
  }, {
    name: 'Help To Choice',
    avatar: 'choice.png',
    actions: [
      {
        title: 'Salutation',
        desc: 'Hello Help To Choice',
        cmds: ['hello', 'Hello', 'HELLO', 'Hi', 'hi'],
        param: '',
        action() {
          return 'Hello, i\'m Help To Choice';
        }
      }, {
        title: 'HelpChoice txt',
        desc: 'Help Choice text',
        cmds: ['choicetxt', 'yntxt'],
        param: '',
        async action() {
          return fetch('https://yesno.wtf/api')
            .then((res) => res.json())
            .then((dataReturn) => `${dataReturn.answer}`)
            .catch((error) => error);
        }
      }, {
        title: 'Help Choice png',
        desc: 'Help Choice Picture',
        cmds: ['choice', 'yn', 'yesno'],
        param: '',
        async action() {
          return fetch('https://yesno.wtf/api')
            .then((res) => res.json())
            .then((dataReturn) => `<img src="${dataReturn.image}" alt="" style="max-width: 220px; max-height: 220px">`)
            .catch((error) => error);
        }
      }
    ]
  }
];

const bots = data.map((bot) => new Bot(bot));
export default new Chat(bots);
