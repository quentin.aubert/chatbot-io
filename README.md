# chatbot-io
A chatbot project for dev courses to using node. 
This is a basic chat with 3 bots which one interract with you with some commands.

## Developpement status
- [x] First HTML & CSS
- [x] Add project serveur (webpack environment)
- [x] Add Classes (Message/Bot/Chat/Action)
- [x] Add 3 Bots (Pokedex, Help to Choice, Meme Generator)

### BOTS
3 Differents Bot in this Chatbot : Pokedex, Meme Generator & Help to Choice

#### Pokedex
| name    | desc                                     | cmds                                    | parameter            | return |
|---------|------------------------------------------|-----------------------------------------|----------------------|--------|
| Hello   | Salutation of Pokedex                    | ['Hello', 'hello', 'Hi', 'hi']          | null                 | String |
| Pokemon | Give some informations about a pokemon   | ['pokemon', 'pkmn', 'pkmninfo', 'info'] | pokemonName - STRING | String |
| Shiny   | Show Shiny Sprite of a Pokémon           | ['Shiny', 'shiny']                      | pokemonName - STRING | String |

#### Meme Generator
| name         | desc                                          | cmds                            | parameter            | return |
|--------------|-----------------------------------------------|---------------------------------|----------------------|--------|
| Hello        | Salutation of Meme Generator                  | ['Hello', 'hello', 'Hi', 'hi']  | null                 | String |
| Meme         | Show a random meme                            | ['Meme', 'meme', 'generate']    | null                 | String |
| Meme Reddit  | Show random meme from specific reddit subject | ['rd', 'Reddit', 'reddit']      | subjectName - STRING | String |

#### Help to choice
| name    | desc                              | cmds                              | parameter       | return |
|---------|-----------------------------------|-----------------------------------|-----------------|--------|
| Hello   | Salutation of Help to Choice      | ['Hello', 'hello', 'Hi', 'hi']    | null            | String |
| Help Choice txt | Help to choice with text  | ['Choicetxt', 'yntxt']            | null            | String |
| Help Choice png | Help to Choice with gif   | ['Choice', 'yn', 'yes or no']     | null            | String |

## Contributor 
[Quentin AUBERT](https://gitlab.com/quentin.aubert)